EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RJ45 J4
U 1 1 594930B7
P 5250 3250
F 0 "J4" H 5326 3867 50  0000 C CNN
F 1 "RJ45" H 5326 3776 50  0000 C CNN
F 2 "" H 5250 3250 50  0001 C CNN
F 3 "" H 5250 3250 50  0001 C CNN
	1    5250 3250
	1    0    0    -1  
$EndComp
$Comp
L RJ45 J3
U 1 1 59493101
P 4200 3250
F 0 "J3" H 4276 3867 50  0000 C CNN
F 1 "RJ45" H 4276 3776 50  0000 C CNN
F 2 "" H 4200 3250 50  0001 C CNN
F 3 "" H 4200 3250 50  0001 C CNN
	1    4200 3250
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 J2
U 1 1 59493806
P 3400 5250
F 0 "J2" H 3319 4675 50  0000 C CNN
F 1 "CONN_01X08" H 3319 4766 50  0000 C CNN
F 2 "" H 3400 5250 50  0001 C CNN
F 3 "" H 3400 5250 50  0001 C CNN
	1    3400 5250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 3700 4900 4900
Wire Wire Line
	4900 4900 3600 4900
Wire Wire Line
	3600 5000 5000 5000
Wire Wire Line
	5000 5000 5000 3700
Wire Wire Line
	5100 3700 5100 5100
Wire Wire Line
	5100 5100 3600 5100
Wire Wire Line
	3600 5200 5200 5200
Wire Wire Line
	5200 5200 5200 3700
Wire Wire Line
	3600 5300 5300 5300
Wire Wire Line
	5300 5300 5300 3700
Wire Wire Line
	3600 5400 5400 5400
Wire Wire Line
	5400 5400 5400 3700
Wire Wire Line
	3600 5500 5500 5500
Wire Wire Line
	5500 5500 5500 3700
Wire Wire Line
	3600 5600 5600 5600
Wire Wire Line
	5600 5600 5600 3700
Wire Wire Line
	5800 2900 5800 2500
Wire Wire Line
	5800 2500 4750 2500
Connection ~ 4750 2900
Wire Wire Line
	4750 2500 4750 4550
$Comp
L CONN_01X08 J1
U 1 1 59493C94
P 3400 4150
F 0 "J1" H 3319 3575 50  0000 C CNN
F 1 "CONN_01X08" H 3319 3666 50  0000 C CNN
F 2 "" H 3400 4150 50  0001 C CNN
F 3 "" H 3400 4150 50  0001 C CNN
	1    3400 4150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 3700 3850 3800
Wire Wire Line
	3850 3800 3600 3800
Wire Wire Line
	3950 3700 3950 3900
Wire Wire Line
	3950 3900 3600 3900
Wire Wire Line
	4050 3700 4050 4000
Wire Wire Line
	4050 4000 3600 4000
Wire Wire Line
	4150 3700 4150 4100
Wire Wire Line
	4150 4100 3600 4100
Wire Wire Line
	4250 3700 4250 4200
Wire Wire Line
	4250 4200 3600 4200
Wire Wire Line
	4350 3700 4350 4300
Wire Wire Line
	4350 4300 3600 4300
Wire Wire Line
	4450 3700 4450 4400
Wire Wire Line
	4450 4400 3600 4400
Wire Wire Line
	4550 3700 4550 4500
Wire Wire Line
	4550 4500 3600 4500
$Comp
L GNDD #PWR1
U 1 1 59493E35
P 4700 4550
F 0 "#PWR1" H 4700 4300 50  0001 C CNN
F 1 "GNDD" H 4704 4395 50  0000 C CNN
F 2 "" H 4700 4550 50  0001 C CNN
F 3 "" H 4700 4550 50  0001 C CNN
	1    4700 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4550 4700 4550
$EndSCHEMATC
