EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:BeagleBone-Black-Cape-cache
LIBS:doragasu
LIBS:nes_ctrl
LIBS:pupnes-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X07 J2
U 1 1 59499092
P 5350 2350
F 0 "J2" H 5428 2391 50  0000 L CNN
F 1 "CONN_01X07" H 5428 2300 50  0000 L CNN
F 2 "" H 5350 2350 50  0001 C CNN
F 3 "" H 5350 2350 50  0001 C CNN
	1    5350 2350
	1    0    0    -1  
$EndComp
Text Label 3800 2650 2    60   ~ 0
DGND
Text Label 3800 2550 2    60   ~ 0
VCC_5v
Text Label 3800 2450 2    60   ~ 0
LATCH
Text Label 3800 2350 2    60   ~ 0
CLOCK
NoConn ~ 5150 2050
NoConn ~ 5150 2250
$Comp
L CONN_01X07 J?
U 1 1 596F749B
P 3150 2350
F 0 "J?" H 3069 1825 50  0000 C CNN
F 1 "CONN_01X07" H 3069 1916 50  0000 C CNN
F 2 "" H 3150 2350 50  0001 C CNN
F 3 "" H 3150 2350 50  0001 C CNN
	1    3150 2350
	-1   0    0    1   
$EndComp
$EndSCHEMATC
