let gpioMock = require('gpio-mock');
let fs = require('fs');

gpioMock.start(function(err) {
  fs.writeFile('/sys/class/gpio/export', '1', function(err) {
    if (!err) {
      // GPIO 1 is exported.
      fs.writeFile('/sys/class/gpio/gpio1/direction', 'out', function(err) {
        if (!err) {
          // GPIO 1 is set to out
          fs.writeFile('/sys/class/gpio/gpio1/value', 1, function(err) {
            // GPIO 1 is set to high
          }
        }
      });
    }
  });

  // Reset changes to fs when done
  gpioMock.stop();
});