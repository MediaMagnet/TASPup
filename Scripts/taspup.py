# Runner script to play from fm2 file

#lib files
import Adafruit_BBIO.GPIO as gpio
import ctypes
import fileinput

#setting c librarys

libc = ctypes.cdll.LoadLibrary('libc.so.6')

#pin names

#player 1

p1latch = "P8_7"
p1clock = "P8_9"
p1data = "P8_11"

#player 2

p2latch = "P8_8"
p2clock = "P8_10"
p2data = "P8_12"

#define pins

def player1():
	gpio.setup(p1latch, gpio.IN)
	gpio.setup(p1clock, gpio.IN)
	gpio.setup(p1data, gpio.OUT)

def player2():
	gpio.setup(p2latch, gpio.IN)
	gpio.setup(p2clock, gpio.IN)
	gpio.setup(p2data, gpio.OUT)
#Delay frame counter

def sleepframe():
	for _ in range(extraframe):
		libc.usleep(16667)
		print("slept a frame")

#Button presses

def keypress():
	if gpio.input(latch) == 1:
		# playback()
		libc.usleep(presleep)
		gpio.output(data, 1)
		libc.usleep(postsleep)

basewait = 18
presleep = 0
postsleep = 0

# def playback():
# 	if '.A|' in line:
# 		presleep = 0
# 		postsleep = basewait + (12 * 6)
# 	elif '.B.' in line:
# 		presleep = basewait
# 		postsleep = 12 * 7
# 	elif '.S.' in line:
# 		presleep = basewait + 12
# 		postsleep = 12 * 6
# 	elif '.T.' in line:
# 		presleep = basewait + (12 * 2)
# 		postsleep = 12 * 5
# 	elif '.U.' in line:
# 		presleep = basewait + (12 * 3)
# 		postsleep = 12 * 4
# 	elif '.D.' in line:
# 		presleep = basewait + (12 * 4)
# 		postsleep = 12 * 3
# 	elif '.L.' in line:
# 		presleep = basewait + (12 * 5)
# 		postsleep = 12 * 2
# 	elif '|R.' in line:
# 		presleep = basewait + (12 * 6)
# 		postsleep = 0
# 	else:
# 		presleep = 0
# 		postsleep = 0

# File reader

print("Welcome to TASPup, let's get things set up, I'll ask you a few questions and then start play back.")

#set moviefile

moviefile = str(input("Please specify the fm2 file you want to read? "))

#set controllers

controllers = str(input("Is this a one player game or a two player game? "))
if controllers == 1:
	print("Enabling controller 1 please make sure you're connected to P8_7 P8_9 and P_11")
	player1()
elif controllers == 2:
	print("Enabling both controllers please make sure you're connected to P8_7 P8_9 and P_11 for player 1 and P8_8 P8_10 and P8_12 for player 2")
	player1()
	player2()

else:
	print("Assuming one controller please make sure you're connected to P8_7 P8_9 and P_11")
	player1()

#set extraframes

extraframe = str(input("Any extra frames to make sync happen? "))
if extraframe is '':
	extraframe = int(0)
else:
	extraframe = int(extraframe)

def movieplay():
	with open(moviefile) as fm2:
		for line in fm2:
			print(line)
			if '.A|' in line:
				presleep = 0
				postsleep = basewait + (12 * 6)
			elif '.B.' in line:
				presleep = basewait
				postsleep = 12 * 7
			elif '.S.' in line:
				presleep = basewait + 12
				postsleep = 12 * 6
			elif '.T.' in line:
				presleep = basewait + (12 * 2)
				postsleep = 12 * 5
			elif '.U.' in line:
				presleep = basewait + (12 * 3)
				postsleep = 12 * 4
			elif '.D.' in line:
				presleep = basewait + (12 * 4)
				postsleep = 12 * 3
			elif '.L.' in line:
				presleep = basewait + (12 * 5)	
				postsleep = 12 * 2
			elif '|R.' in line:
				presleep = basewait + (12 * 6)
				postsleep = 0
			else:
				presleep = 0
				postsleep = 0
			keypress()
			print(presleep)
			print(postsleep)

print('Begining movie playback')
if extraframe > 0:
	sleepframe()
	movieplay()
elif extraframe == 0:
	movieplay()
else:
	print("lol")



# |0|RLDUSTBA|||
#def playback():
#	if line == '|0|.......A|||':
#		presleep = 0
#		postsleep = 90
#	elif line == '|0|......B.|||':
#		presleep = 18
#		postsleep = 72
#	elif line == '|0|.....T..|||':
#		presleep = 30
#		postsleep = 60
#	elif line == '|0|....S...|||':
#		presleep = 66
#		postsleep = 48
#	elif line == '|0|...U....|||':
#		presleep = 48
#		postsleep = 66
#	elif line == '|0|..D.....|||':
#		presleep = 60
#		postsleep = 30
#	elif line == '|0|.L......|||':
#		presleep = 72
#		postsleep = 18
#	elif line == '|0|
