EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:nes_ctrl
LIBS:MiscellaneousDevices
LIBS:switches
LIBS:mutli-tas-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "TASPup Cross-Compatibility Board"
Date "2017-06-28"
Rev "Rev 1"
Comp "MediaMagnet"
Comment1 ""
Comment2 ""
Comment3 "NES Controller, TASPup and TAS-Link/PSOC connections"
Comment4 "Cross compatible board allows link of:"
$EndDescr
$Comp
L RJ45 O2
U 1 1 59527BAF
P 7250 3250
F 0 "O2" V 7280 3730 50  0000 L CNN
F 1 "RJ45" V 7371 3730 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 7250 3250 50  0001 C CNN
F 3 "" H 7250 3250 50  0001 C CNN
	1    7250 3250
	0    1    1    0   
$EndComp
$Comp
L RJ45 O1
U 1 1 59527F3A
P 7250 4300
F 0 "O1" V 7280 4780 50  0000 L CNN
F 1 "RJ45" V 7371 4780 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 7250 4300 50  0001 C CNN
F 3 "" H 7250 4300 50  0001 C CNN
	1    7250 4300
	0    1    1    0   
$EndComp
$Comp
L NES_CTRL P21
U 1 1 59527FFE
P 2100 1950
F 0 "P21" H 2100 2037 60  0000 C CNN
F 1 "NES_CTRL" H 2100 1931 60  0000 C CNN
F 2 "NES_CTRL:nes_ctrl" H 2100 1900 60  0001 C CNN
F 3 "" H 2100 1900 60  0001 C CNN
	1    2100 1950
	1    0    0    -1  
$EndComp
$Comp
L RJ45 P13
U 1 1 5952928C
P 6250 5150
F 0 "P13" V 6280 5630 50  0000 L CNN
F 1 "RJ45" V 6371 5630 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 6250 5150 50  0001 C CNN
F 3 "" H 6250 5150 50  0001 C CNN
	1    6250 5150
	-1   0    0    1   
$EndComp
$Comp
L RJ45 P23
U 1 1 59529293
P 6250 2350
F 0 "P23" V 6373 2830 50  0000 L CNN
F 1 "RJ45" V 6282 2830 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 6250 2350 50  0001 C CNN
F 3 "" H 6250 2350 50  0001 C CNN
	1    6250 2350
	1    0    0    -1  
$EndComp
Text Notes 7900 3900 1    60   ~ 0
Output
$Comp
L GND #PWR01
U 1 1 5952FB01
P 1650 4050
F 0 "#PWR01" H 1650 3800 50  0001 C CNN
F 1 "GND" V 1655 3922 50  0000 R CNN
F 2 "" H 1650 4050 50  0001 C CNN
F 3 "" H 1650 4050 50  0001 C CNN
	1    1650 4050
	0    1    1    0   
$EndComp
$Comp
L NES_CTRL P11
U 1 1 595280C8
P 2650 4050
F 0 "P11" H 2650 4137 60  0000 C CNN
F 1 "NES_CTRL" H 2650 4031 60  0000 C CNN
F 2 "NES_CTRL:nes_ctrl" H 2650 4000 60  0001 C CNN
F 3 "" H 2650 4000 60  0001 C CNN
	1    2650 4050
	1    0    0    -1  
$EndComp
Text Notes 1950 6400 0    60   ~ 0
P1 Connector Side
Text Notes 1600 1350 0    60   ~ 0
P2 Connector Side\n
$Comp
L RJ45 P?
U 1 1 5AA0302B
P 5150 5150
F 0 "P?" V 5180 5630 50  0000 L CNN
F 1 "RJ45" V 5271 5630 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 5150 5150 50  0001 C CNN
F 3 "" H 5150 5150 50  0001 C CNN
	1    5150 5150
	-1   0    0    1   
$EndComp
$Comp
L RJ45 P?
U 1 1 5AA03105
P 5200 2350
F 0 "P?" V 5323 2830 50  0000 L CNN
F 1 "RJ45" V 5232 2830 50  0000 L CNN
F 2 "Connectors:RJ45_8" H 5200 2350 50  0001 C CNN
F 3 "" H 5200 2350 50  0001 C CNN
	1    5200 2350
	1    0    0    -1  
$EndComp
$Comp
L SW_Rotary3x4 SW?
U 1 1 5AA0348D
P 3900 3700
F 0 "SW?" H 3950 4590 50  0000 C CNN
F 1 "SW_Rotary3x4" H 3950 4499 50  0000 C CNN
F 2 "" H 3800 4500 50  0001 C CNN
F 3 "" H 3800 4500 50  0001 C CNN
	1    3900 3700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
