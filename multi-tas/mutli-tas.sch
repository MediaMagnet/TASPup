EESchema Schematic File Version 4
LIBS:mutli-tas-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "TASPup Cross-Compatibility Board"
Date "2017-06-28"
Rev "Rev 1"
Comp "MediaMagnet"
Comment1 ""
Comment2 ""
Comment3 "NES Controller, TASPup and TAS-Link/PSOC connections"
Comment4 "Cross compatible board allows link of:"
$EndDescr
$Comp
L conn:RJ45 O2
U 1 1 59527BAF
P 7250 3250
F 0 "O2" V 7280 3730 50  0000 L CNN
F 1 "RJ45" V 7371 3730 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 7250 3250 50  0001 C CNN
F 3 "" H 7250 3250 50  0001 C CNN
	1    7250 3250
	0    1    1    0   
$EndComp
$Comp
L conn:RJ45 O1
U 1 1 59527F3A
P 7250 4250
F 0 "O1" V 7280 4730 50  0000 L CNN
F 1 "RJ45" V 7371 4730 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 7250 4250 50  0001 C CNN
F 3 "" H 7250 4250 50  0001 C CNN
	1    7250 4250
	0    1    1    0   
$EndComp
$Comp
L nes_ctrl:NES_CTRL P21
U 1 1 59527FFE
P 4300 1950
F 0 "P21" H 4300 2037 60  0000 C CNN
F 1 "NES_CTRL" H 4300 1931 60  0000 C CNN
F 2 "NES_CTRL:nes_ctrl" H 4300 1900 60  0001 C CNN
F 3 "" H 4300 1900 60  0001 C CNN
	1    4300 1950
	1    0    0    -1  
$EndComp
$Comp
L conn:RJ45 P13
U 1 1 5952928C
P 6250 5150
F 0 "P13" V 6280 5630 50  0000 L CNN
F 1 "RJ45" V 6371 5630 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 6250 5150 50  0001 C CNN
F 3 "" H 6250 5150 50  0001 C CNN
	1    6250 5150
	-1   0    0    1   
$EndComp
$Comp
L conn:RJ45 P23
U 1 1 59529293
P 6250 2350
F 0 "P23" V 6373 2830 50  0000 L CNN
F 1 "RJ45" V 6282 2830 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 6250 2350 50  0001 C CNN
F 3 "" H 6250 2350 50  0001 C CNN
	1    6250 2350
	1    0    0    -1  
$EndComp
Text Notes 7900 3900 1    60   ~ 0
Output
$Comp
L power1:GND #PWR01
U 1 1 5952FB01
P 2400 4000
F 0 "#PWR01" H 2400 3750 50  0001 C CNN
F 1 "GND" V 2405 3872 50  0000 R CNN
F 2 "" H 2400 4000 50  0001 C CNN
F 3 "" H 2400 4000 50  0001 C CNN
	1    2400 4000
	0    1    1    0   
$EndComp
$Comp
L nes_ctrl:NES_CTRL P11
U 1 1 595280C8
P 4250 4850
F 0 "P11" H 4250 4937 60  0000 C CNN
F 1 "NES_CTRL" H 4250 4831 60  0000 C CNN
F 2 "NES_CTRL:nes_ctrl" H 4250 4800 60  0001 C CNN
F 3 "" H 4250 4800 60  0001 C CNN
	1    4250 4850
	1    0    0    -1  
$EndComp
Text Notes 5200 5950 0    60   ~ 0
P1 Connector Side
Text Notes 6850 2650 0    60   ~ 0
P2 Connector Side\n
$Comp
L conn:RJ45 P1
U 1 1 5AA0302B
P 5150 5150
F 0 "P1" V 5180 5630 50  0000 L CNN
F 1 "RJ45" V 5271 5630 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 5150 5150 50  0001 C CNN
F 3 "" H 5150 5150 50  0001 C CNN
	1    5150 5150
	-1   0    0    1   
$EndComp
$Comp
L conn:RJ45 P2
U 1 1 5AA03105
P 5200 2350
F 0 "P2" V 5323 2830 50  0000 L CNN
F 1 "RJ45" V 5232 2830 50  0000 L CNN
F 2 "Con_RJ:RJ45_8_S" H 5200 2350 50  0001 C CNN
F 3 "" H 5200 2350 50  0001 C CNN
	1    5200 2350
	1    0    0    -1  
$EndComp
$Comp
L nes_ctrl:SW_rotary1x4 SW1
U 1 1 5AA03423
P 5350 3600
F 0 "SW1" H 5250 3825 50  0000 C CNN
F 1 "SW_rotary1x4" H 5250 3734 50  0000 C CNN
F 2 "NES_CTRL:SW_rotary1x4" H 5350 3700 50  0001 C CNN
F 3 "" H 5350 3700 50  0001 C CNN
	1    5350 3600
	-1   0    0    -1  
$EndComp
NoConn ~ 4900 4700
NoConn ~ 6800 4500
NoConn ~ 6800 3500
NoConn ~ 6500 2800
NoConn ~ 5450 2800
NoConn ~ 6000 4700
$Comp
L nes_ctrl:SNES_CTRL P22
U 1 1 5AA03B6D
P 3250 2250
F 0 "P22" H 3065 2387 60  0000 C CNN
F 1 "SNES_CTRL" H 3065 2281 60  0000 C CNN
F 2 "NES_CTRL:SNES_CTRL" H 3200 1150 60  0001 C CNN
F 3 "" H 3200 1150 60  0001 C CNN
	1    3250 2250
	1    0    0    -1  
$EndComp
$Comp
L nes_ctrl:SNES_CTRL P12
U 1 1 5AA03CDE
P 3050 5400
F 0 "P12" H 2865 5537 60  0000 C CNN
F 1 "SNES_CTRL" H 2865 5431 60  0000 C CNN
F 2 "NES_CTRL:SNES_CTRL" H 3000 4300 60  0001 C CNN
F 3 "" H 3000 4300 60  0001 C CNN
	1    3050 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7800 3800 7800 2000
Wire Wire Line
	7800 2000 6800 2000
Wire Wire Line
	6800 2900 6800 2000
Connection ~ 6800 2000
Wire Wire Line
	6800 2000 6800 1800
Wire Wire Line
	6800 1800 5800 1800
Wire Wire Line
	2400 1800 2400 4000
Wire Wire Line
	2400 5000 2650 5000
Connection ~ 2400 4000
Wire Wire Line
	2400 4000 2400 5000
Wire Wire Line
	3650 2650 3850 2650
Wire Wire Line
	3850 2650 3850 1800
Connection ~ 3850 1800
Wire Wire Line
	3850 1800 2400 1800
Wire Wire Line
	4000 2150 4000 1800
Connection ~ 4000 1800
Wire Wire Line
	4000 1800 3850 1800
Wire Wire Line
	5750 2000 5750 1800
Connection ~ 5750 1800
Wire Wire Line
	5750 1800 4750 1800
Wire Wire Line
	7600 4800 7600 5800
Wire Wire Line
	7600 5800 6700 5800
Wire Wire Line
	2400 5800 2400 5000
Connection ~ 2400 5000
Wire Wire Line
	3950 5050 3750 5050
Wire Wire Line
	3750 5050 3750 5800
Connection ~ 3750 5800
Wire Wire Line
	3750 5800 2400 5800
Wire Wire Line
	5700 5500 5700 5800
Connection ~ 5700 5800
Wire Wire Line
	6600 4700 6700 4700
Wire Wire Line
	6700 4700 6700 5800
Connection ~ 6700 5800
Wire Wire Line
	6700 5800 5700 5800
Wire Wire Line
	5500 4700 5700 4700
Wire Wire Line
	5700 4700 5700 5500
Connection ~ 5700 5500
Wire Wire Line
	5900 2800 5800 2800
Wire Wire Line
	5800 2800 5800 1800
Connection ~ 5800 1800
Wire Wire Line
	5800 1800 5750 1800
Wire Wire Line
	4850 2800 4750 2800
Wire Wire Line
	4750 2800 4750 1800
Connection ~ 4750 1800
Wire Wire Line
	4750 1800 4000 1800
Wire Wire Line
	6800 3900 6800 3800
Wire Wire Line
	6800 3800 7600 3800
Connection ~ 7600 3800
Wire Wire Line
	7600 3800 7800 3800
Wire Wire Line
	6500 4700 6500 3850
Wire Wire Line
	6500 3850 6000 3850
Wire Wire Line
	6000 2800 6000 3850
Wire Wire Line
	4500 2300 4700 2300
Wire Wire Line
	2700 2650 2700 3600
Wire Wire Line
	2700 3600 3600 3600
Wire Wire Line
	3600 5000 3600 3600
Wire Wire Line
	6800 3100 6100 3100
Wire Wire Line
	3500 3100 3500 2650
Wire Wire Line
	4500 2600 4500 3100
Connection ~ 4500 3100
Wire Wire Line
	4500 3100 3500 3100
Wire Wire Line
	5050 2800 5050 3100
Connection ~ 5050 3100
Wire Wire Line
	5050 3100 4500 3100
Wire Wire Line
	6100 2800 6100 3100
Connection ~ 6100 3100
Wire Wire Line
	6100 3100 5050 3100
Wire Wire Line
	6800 3200 6200 3200
Wire Wire Line
	3150 3200 3150 2650
Wire Wire Line
	4000 2600 4000 3200
Connection ~ 4000 3200
Wire Wire Line
	4000 3200 3150 3200
Wire Wire Line
	5150 2800 5150 3200
Connection ~ 5150 3200
Wire Wire Line
	5150 3200 4000 3200
Wire Wire Line
	6200 2800 6200 3200
Connection ~ 6200 3200
Wire Wire Line
	6200 3200 5150 3200
Wire Wire Line
	6800 3300 6300 3300
Wire Wire Line
	3350 3300 3350 2650
Wire Wire Line
	4500 2450 4550 2450
Wire Wire Line
	4550 2450 4550 3300
Connection ~ 4550 3300
Wire Wire Line
	4550 3300 3350 3300
Wire Wire Line
	5250 2800 5250 3300
Connection ~ 5250 3300
Wire Wire Line
	5250 3300 4550 3300
Wire Wire Line
	6300 2800 6300 3300
Connection ~ 6300 3300
Wire Wire Line
	6300 3300 5250 3300
Wire Wire Line
	6800 3400 6400 3400
Wire Wire Line
	3000 3400 3000 2650
Wire Wire Line
	3900 2450 3900 3400
Connection ~ 3900 3400
Wire Wire Line
	3900 3400 3000 3400
Wire Wire Line
	3900 2450 4000 2450
Wire Wire Line
	5350 2800 5350 3400
Connection ~ 5350 3400
Wire Wire Line
	5350 3400 3900 3400
Wire Wire Line
	6400 2800 6400 3400
Connection ~ 6400 3400
Wire Wire Line
	6400 3400 5350 3400
Wire Wire Line
	6800 3600 6600 3600
Wire Wire Line
	6150 3600 6150 3500
Wire Wire Line
	6150 3500 5550 3500
Wire Wire Line
	2850 3500 2850 2650
Wire Wire Line
	4000 2300 3950 2300
Wire Wire Line
	3950 2300 3950 3500
Connection ~ 3950 3500
Wire Wire Line
	3950 3500 2850 3500
Wire Wire Line
	5550 2800 5550 3500
Connection ~ 5550 3500
Wire Wire Line
	5550 3500 3950 3500
Connection ~ 6600 3600
Wire Wire Line
	6600 3600 6150 3600
Wire Wire Line
	6800 4100 6400 4100
Wire Wire Line
	2800 4100 2800 5000
Wire Wire Line
	3750 5800 4600 5800
Wire Wire Line
	4600 5500 4600 5800
Connection ~ 4600 5800
Wire Wire Line
	4600 5800 5700 5800
Wire Wire Line
	4450 5500 4500 5500
Connection ~ 4500 4100
Wire Wire Line
	4500 4100 2800 4100
Wire Wire Line
	5300 4700 5300 4100
Connection ~ 5300 4100
Wire Wire Line
	5300 4100 4500 4100
Wire Wire Line
	6400 4700 6400 4100
Connection ~ 6400 4100
Wire Wire Line
	6400 4100 5300 4100
$Comp
L mutli-tas-rescue:LED_Small-Device D1
U 1 1 5AA9B610
P 4900 3600
F 0 "D1" H 4750 3600 50  0000 L TNN
F 1 "LED_Small" V 4945 3698 50  0001 L CNN
F 2 "LED_THT:LED_D3.0mm" V 4900 3600 50  0001 C CNN
F 3 "~" V 4900 3600 50  0001 C CNN
	1    4900 3600
	-1   0    0    1   
$EndComp
$Comp
L mutli-tas-rescue:LED_Small-Device D2
U 1 1 5AA9B78A
P 4900 3700
F 0 "D2" H 4750 3700 50  0000 L TNN
F 1 "LED_Small" V 4945 3798 50  0001 L CNN
F 2 "LED_THT:LED_D3.0mm" V 4900 3700 50  0001 C CNN
F 3 "~" V 4900 3700 50  0001 C CNN
	1    4900 3700
	-1   0    0    1   
$EndComp
$Comp
L mutli-tas-rescue:LED_Small-Device D3
U 1 1 5AA9B7D9
P 4900 3800
F 0 "D3" H 4750 3800 50  0000 L TNN
F 1 "LED_Small" V 4945 3898 50  0001 L CNN
F 2 "LED_THT:LED_D3.0mm" V 4900 3800 50  0001 C CNN
F 3 "~" V 4900 3800 50  0001 C CNN
	1    4900 3800
	-1   0    0    1   
$EndComp
$Comp
L mutli-tas-rescue:LED_Small-Device D4
U 1 1 5AA9B807
P 4900 3900
F 0 "D4" H 4750 3900 50  0000 L TNN
F 1 "LED_Small" V 4945 3998 50  0001 L CNN
F 2 "LED_THT:LED_D3.0mm" V 4900 3900 50  0001 C CNN
F 3 "~" V 4900 3900 50  0001 C CNN
	1    4900 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	5900 3800 6700 3800
Wire Wire Line
	6700 3800 6700 4000
Wire Wire Line
	6700 4000 6800 4000
Wire Wire Line
	6800 3000 6700 3000
Wire Wire Line
	6700 3000 6700 3800
Connection ~ 6700 3800
Wire Wire Line
	6000 3850 4800 3850
Wire Wire Line
	4800 3850 4800 3900
Connection ~ 6000 3850
Wire Wire Line
	5400 4700 5400 3750
Wire Wire Line
	5400 3750 5350 3750
Wire Wire Line
	4800 3750 4800 3800
Wire Wire Line
	4950 2800 4950 3550
Wire Wire Line
	4950 3550 5350 3550
Wire Wire Line
	5350 3550 5350 3750
Connection ~ 5350 3750
Wire Wire Line
	5350 3750 4800 3750
Wire Wire Line
	3600 3600 4800 3600
Connection ~ 3600 3600
Wire Wire Line
	6800 4200 6300 4200
Wire Wire Line
	3150 4200 3150 5000
Wire Wire Line
	3950 5500 3800 5500
Wire Wire Line
	3800 5500 3800 4200
Connection ~ 3800 4200
Wire Wire Line
	3800 4200 3150 4200
Wire Wire Line
	5200 4700 5200 4200
Connection ~ 5200 4200
Wire Wire Line
	5200 4200 3800 4200
Wire Wire Line
	6300 4700 6300 4200
Connection ~ 6300 4200
Wire Wire Line
	6300 4200 5200 4200
Wire Wire Line
	6800 4300 6200 4300
Wire Wire Line
	2950 4300 2950 5000
Wire Wire Line
	4600 5350 4600 4300
Connection ~ 4600 4300
Wire Wire Line
	4600 4300 2950 4300
Wire Wire Line
	5100 4700 5100 4300
Connection ~ 5100 4300
Wire Wire Line
	5100 4300 4600 4300
Wire Wire Line
	6200 4700 6200 4300
Connection ~ 6200 4300
Wire Wire Line
	6200 4300 5100 4300
Wire Wire Line
	6800 4400 6100 4400
Wire Wire Line
	3300 4400 3300 5000
Wire Wire Line
	3950 5350 3900 5350
Wire Wire Line
	3900 5350 3900 4400
Connection ~ 3900 4400
Wire Wire Line
	3900 4400 3300 4400
Wire Wire Line
	5000 4700 5000 4400
Connection ~ 5000 4400
Wire Wire Line
	5000 4400 3900 4400
Wire Wire Line
	6100 4700 6100 4400
Connection ~ 6100 4400
Wire Wire Line
	6100 4400 5000 4400
Wire Wire Line
	6800 4600 5900 4600
Wire Wire Line
	3450 4600 3450 5000
Wire Wire Line
	3950 5200 3850 5200
Wire Wire Line
	3850 5200 3850 4600
Connection ~ 3850 4600
Wire Wire Line
	3850 4600 3450 4600
Wire Wire Line
	4800 4700 4800 4600
Connection ~ 4800 4600
Wire Wire Line
	4800 4600 3850 4600
Wire Wire Line
	5900 4700 5900 4600
Connection ~ 5900 4600
Wire Wire Line
	5900 4600 4800 4600
Wire Wire Line
	4500 4100 4500 5500
Wire Wire Line
	4600 5350 4450 5350
Wire Wire Line
	6600 2800 6600 3600
Wire Wire Line
	4450 5200 4700 5200
Wire Wire Line
	4700 2300 4700 3700
Wire Wire Line
	4800 3700 4700 3700
Connection ~ 4700 3700
Wire Wire Line
	4700 3700 4700 5200
$EndSCHEMATC
